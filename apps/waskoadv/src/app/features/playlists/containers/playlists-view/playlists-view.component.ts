import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { Playlist } from 'apps/waskoadv/src/app/core/model/Playlist';
import { mockPlaylists } from './mockPlaylists';

enum Modes {
  details = 'details',
  edit = 'edit'
}
@Component({
  selector: 'wasko-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss'],
  // providers: [
  //     {
  //       provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
  //       useValue: {
  //         appearance: 'fill', 
  //         hideRequiredMarker: false
  //       }
  //     }
  //   ]
})
export class PlaylistsViewComponent implements OnInit {

  mode: Modes = Modes.details
  // mode: 'details' | 'edit' = Modes.details
  
  constructor(
    @Inject('INITIAL_PLAYLISTS') @Optional() public playlists: Playlist[] 
  ) { 
    this.playlists = playlists || mockPlaylists
  }


  selectedPlaylist?: Playlist

  // selectPlaylist(playlist_id: string) {
  selectPlaylist(playlist_id: Playlist['id']) {  
    this.selectedPlaylist = this.playlists.find(p => p.id === playlist_id)
  }

  editMode() { this.mode = Modes.edit }
  detailsMode() { this.mode = Modes.details }

  savePlaylist(draft: Playlist) {
    console.log(draft);

    // Mutable - OnPush won't detect change!!!
    // const index = this.playlists.findIndex(p => p.id === draft.id)
    // if(index !== -1)
    // this.playlists.splice(index, 1, draft)

    // Immutable - OnPush will detect change
    this.playlists = this.playlists.map(p => p.id === draft.id ? draft : p)
  }

  removePlaylist(id: Playlist['id']) {
    // const index = this.playlists.findIndex(p => p.id === id)
    // if(index !== -1)
    // this.playlists.splice(index, 1)
    // this.playlists = [...this.playlists]

    // const index = this.playlists.findIndex(p => p.id === id)
    // this.playlists = [...this.playlists.slice(0, index - 1), ...this.playlists.slice(index)]

    this.playlists = this.playlists.filter(p => p.id !== id)
  }

  addPlaylist() {
    // this.selectedPlaylist.name // Object is possibly 'undefined'

    // Function Guard
    if (!this.selectedPlaylist) { return }

    this.selectedPlaylist = {
      ...this.selectedPlaylist,
      name: Date.now().toString()
      // public: !this.selectedPlaylist.public
    }
  }

  ngOnInit(): void {
  }

}
