import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppComponent } from './app.component';
import { AuthService } from './core/services/auth.service';
import { RouterTestingModule } from '@angular/router/testing'
import { MatListModule } from '@angular/material/list';
// jest.mock('./core/services/auth.service')


describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>

  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      // Shallow rendering:
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        // MatListModule
        RouterTestingModule.withRoutes(
          []
          // [{ path: '', component: BlankCmp }, { path: 'simple', component: SimpleCmp }]
        )
      ],
      providers: [
        {
          provide: AuthService,
          useValue: {
            login: jest.fn()
          }
        },
        {
          provide: MatSnackBar,
          useValue: {
            open: jest.fn()
          }
        },
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
  });

  it('should create the app', () => {
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'waskoadv'`, () => {
    const app = fixture.componentInstance;
    expect(app.title).toEqual('MusicApp');
  });

  it('should render title', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('MusicApp');
  });
  it('should login when login button clicked', () => {
    const compiled = fixture.nativeElement;
    const btnElem = compiled.querySelector('[data-testid="loginbtn"]');

    // btnElem.dispatchEvent(new MouseEvent('click', { bubbles: true, buttons: 1 }))
    btnElem.click()

    const service = TestBed.inject(AuthService)
    
    expect(service.login).toHaveBeenCalled()

    const location = TestBed.inject(Location)
    expect(location.pathname).toEqual('/music-search')
  });
});
