import { Playlist } from 'apps/waskoadv/src/app/core/model/Playlist';

export const mockPlaylists: Playlist[] = [{
  id: '123',
  name: 'Playlist 123',
  public: false,
  type: 'playlist',
  description: 'Best playlist'
}, {
  id: '234',
  name: 'Playlist 234',
  public: true,
  type: 'playlist',
  description: 'Best playlist'
}, {
  id: '345',
  name: 'Playlist 345',
  public: false,
  type: 'playlist',
  description: 'Best playlist'
}];
