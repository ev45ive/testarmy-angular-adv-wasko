import { ComponentFixture, TestBed } from '@angular/core/testing';
import { mockPlaylists } from '../../containers/playlists-view/mockPlaylists';

import { PlaylistDetailsComponent } from './playlist-details.component';

describe('PlaylistDetailsComponent', () => {
  let component: PlaylistDetailsComponent;
  let fixture: ComponentFixture<PlaylistDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlaylistDetailsComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistDetailsComponent);
    component = fixture.componentInstance;
    component.playlist = mockPlaylists[0]
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create', () => {
    // expect(fixture.nativeElement.querySelector('')).toBeTruthy();
    expect(fixture.nativeElement.innerText).toMatch(mockPlaylists[0].name);
  });
});
