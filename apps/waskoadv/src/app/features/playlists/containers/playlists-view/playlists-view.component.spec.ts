import { Component, CUSTOM_ELEMENTS_SCHEMA, Input } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { PlaylistListComponent } from '../../components/playlist-list/playlist-list.component';
import { mockPlaylists } from './mockPlaylists';
import { PlaylistsViewComponent } from './playlists-view.component';

@Component({
  selector: 'wasko-playlist-details',
  template: `<div data-testid="MOCK_DETAILS"></div>`
})
class PlaylistDetailsComponent {
  @Input() playlist: any
}

describe('PlaylistsViewComponent', () => {
  let component: PlaylistsViewComponent;
  let fixture: ComponentFixture<PlaylistsViewComponent>;
  let element: HTMLElement

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [
        PlaylistsViewComponent,
        PlaylistListComponent,
        PlaylistDetailsComponent
      ],
      providers: [
        {
          provide: 'INITIAL_PLAYLISTS',
          useValue: mockPlaylists
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsViewComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should show playlists', () => {
    const itemsEls = element.querySelectorAll<HTMLElement>('wasko-playlist-list-item');
    expect(itemsEls).toHaveLength(mockPlaylists.length);
    expect(itemsEls[0].textContent).toMatch(mockPlaylists[0].name);
  });

  it('should select playlist', waitForAsync(() => {


    console.log(element.innerHTML);
    const itemsEls = element.querySelectorAll<HTMLElement>('wasko-playlist-list-item');
    expect(itemsEls).toHaveLength(mockPlaylists.length);


    itemsEls[0].click()
    // itemsEls[0].dispatchEvent(new MouseEvent('click', { bubbles: true }))
    fixture.detectChanges();


    // expect(element.querySelector('[data-testid="MOCK_DETAILS"]')).toBeTruthy()
    const details = fixture.debugElement.query(By.directive(PlaylistDetailsComponent))
    expect(details).toBeTruthy()
    expect(details.componentInstance.playlist).toEqual(mockPlaylists[0])
  }));
});
